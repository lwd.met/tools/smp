#!/bin/bash

#####################################################################################
#  Copyright 2019 Lawinenwarndienst Tirol / Avalanche Warning Service Tyrol         #
#####################################################################################
#  This is free software: you can redistribute and/or modify it under the terms     #
#  of the MIT license: https://opensource.org/licenses/MIT                          #
#####################################################################################
#
#Convert snowmicropyn output to niViz compatible format:
#     1) Remove the air gap from measurements
#     2) Convert to cm
#     3) Thin out data to managable size
#     4) Project a measurement perpendicular to the snow surface to vertical
#     5) Stretch data for manual projection
#     6) Remove Windows newlines
#
#Convert snowmicropyn output to niViz input:
#    ./smp2niviz.sh SM45M0027_samples.csv
#Other flags and doc see: https://gitlab.com/lwd.met/tools/smp/wikis/home
#
#Michael Reisecker, 2019-01


echo_less() { #be verbose for debugging if requested (-v or true in 2nd argument)
	if [ "$verbose_output" = true -o "$2" = true ]; then
		echo "$1"
	fi
}

prepare_file() {

	#check file name for standard snowmicropyn output
	if [ ! $force = true ]; then #override and convert anyway
		if [ $(echo "$1" | tail -c 13) != "_samples.csv" ]; then
			return
		fi
	fi

	echo_less $(printf -- "-%.0s" {1..40}) #print line
	echo_less "- Preparing file $1..."

	#check first character of input file, which will be ; if it is already an output of this script:
	header=$(head -n 1 "$1")
	if [ ${header::1} = ";" ]; then
		echo_less "- File seems to be converted already. Stopping."
		return
	fi

	if [ "$remove_newlines" = true ]; then #remove Windows newlines
		osstr="$(uname)"
		if [ ${osstr:0:6} = "Darwin" ]; then #OSX
			tr -d '\r' < "$1" > "$1".part
			mv "${1}.part" "$1"
		elif [ ${osstr:0:5} = "Linux" ]; then
			sed -i 's/\r//g' "$1"
		fi
	fi

	#if the input file has _samples.csv in its name, this will be replaced by _niviz.csv. If not, it is appended:
	name_end=$(echo "$1" | tail -c 13)
	if [ "$name_end" = "_samples.csv" ]; then #workaround for ${::-12} for OSX
		basename=$(echo "$1" | rev | cut -c 13- | rev)
	else #extensions != 3 characters are not handled - but let's not clutter this
		basename=$(echo "$1" | rev | cut -c 5- | rev)
	fi

	outfile="${basename}_niviz.csv"
	metafile="${basename}_meta.csv"

	#if there is metadata available, look for "marker_surface" and pass this value
	#(to be substracted from the measurement heights):
	if [ -f "$metafile" ]; then
		echo_less "- Extracting snow surface height from header..."
		surface_offset=$(grep -nr "marker_surface" "$metafile" | cut -d"," -f2) #delimiter , and field 2
		if [ -z "$surface_offset" ]; then
			surface_offset=0
		fi
		echo_less "- Applying offset of ${surface_offset}"
	else
		echo_less "- [No header file found]"
	fi

	echo_less "- Running through dataset..."
	if [ $modno -ne 1 ]; then
		echo_less "  [Thinning data: $modno]"
	else
		echo_less "  [Printing all data]"
	fi
	if [ $slope_angle -ne 0 ]; then
		echo_less "  [Reprojecting to vertical profile]"
	fi

	awk -F "," -v mod=$modno -v offset=$surface_offset -v angle=$slope_angle -v factor=$height_multiplier '

		function mm2cm(mm) {
			return mm/10
		}
		function substract_offset(mm) {
			return mm-offset	
		}
		function project_vertical(xx) {
			return xx/cos(angle*PI/180)		
		}
	
		BEGIN { 
			OFS = "," 
			lcount = 0
			PI = atan2(0, -1)
		}

		#re-print headers with a comment and new unit:
		NR==1 { print ";"$0" and processed by smp2niviz.sh" }
		NR==2 { print ";distance [cm]", $2 }
		NR>2 {
			#print every mod-th line converted to cm etc.:
			if ($1 > offset) {		
				if (lcount % mod == 0) {
					print factor*mm2cm( project_vertical(substract_offset($1)) ), $2
				}
			}
			lcount++
		}

	' "$1" > "$outfile"

	echo_less "- Output written to $outfile"

} #end function

### MAIN PROGRAM ###

set -e #abort on errors

file_name=""
modno=150 #default: output every 150th line
slope_angle=0 #default: slope is flat
height_multiplier=1 #stretch data
verbose_output=false
remove_newlines=false
force=false
scan_folder=false

while [ $# -gt 0 ]; do
	case "$1" in
		-a|--angle)
			slope_angle=$2
			shift 2
			;;
		-d|--directory)
			scan_folder=true
			shift 1
			;;
		-f|--force)
			force=true
			shift 1
			;;
		-l|--newlines)
			remove_newlines=true
			shift 1
			;;
		-m|--multiplier)
			height_multiplier=$2
			shift 2
			;;
		-n|--number)
			modno=$2
			shift 2
			;;
		-v|--verbose)
			verbose_output=true
			shift 1
			;;
		-*)
			echo_less "- Unexpected argument: \"$arg\"" true
			exit 1
			;;
		*)
			file_name="$1" #file or "folder"
			shift 1
			;; 
	esac
done

echo_less "- This is $0"

if [ ${#file_name} -eq 0 ]; then
	echo_less "- ERROR: Provide a filename in the command line. Ex.: ./smp2niviz.sh S45M0027_samples.csv" true
	exit 1
fi

if [ $scan_folder = true ]; then
	echo_less "- Scanning FOLDER with file pattern..."
	for file in $file_name; do
		if [ -f "$file" ]; then #skip folders
			prepare_file "$file"
		fi
	done
else
	echo_less "- Scanning FILE..."
	prepare_file "$file_name"
fi

echo_less "- Done"
