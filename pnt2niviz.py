#!/usr/bin/python3

#####################################################################################
#  Copyright 2019 Lawinenwarndienst Tirol / Avalanche Warning Service Tyrol         #
#####################################################################################
#  This is free software: you can redistribute and/or modify it under the terms     #
#  of the MIT license: https://opensource.org/licenses/MIT                          #
#####################################################################################

#Python script controlling snowmicropyn to get niViz-ready output.
#The syntax is the same as for smp2niviz.sh. Example: python3 pnt2niviz.py S45M0027.pnt -v
#Michael Reisecker, 2019-02


from snowmicropyn import Profile #pip3 install -U snowmicropyn
import pathlib
import sys, subprocess

### SETTINGS ###
meta_export = True
derivatives_export = True
set_markers = True
###

arg = sys.argv #command line parameters (will be passed to the script)
if len(arg) == 1:
	print('# No input files specified. Stopping.')
	exit()

arg[0] = './smp2niviz.sh' #for subprocess

be_verbose = '-v' in arg or '--verbose' in arg
if be_verbose: print('# This is ' + __file__)

for ff in pathlib.Path('.').glob(sys.argv[1]): #run through .pnt-files
	if ff.suffix == '.pnt':
		if be_verbose: print('# Looking at file ' + str(ff) + '...')
		P = Profile.load(ff)
		if be_verbose: print('# Opened profile from ' + str(P.timestamp))

		if set_markers: P.detect_surface() #also sets the marker		
		P.export_samples()
		if meta_export: P.export_meta(include_pnt_header=True) #also include raw pnt header fields
		if derivatives_export: P.export_derivatives()

		#1st item in list is program to call, all others will be passed as command line args:
		arg[1] = str(ff.stem) + '_samples.csv' #switch from pattern matching to single file as globbed here
		if be_verbose: print('# Invoking script for ' + str(ff.stem) + '_samples.csv...')
		subprocess.call(arg) #e. g. ./smp2niviz.sh S450027_samples.csv -v -a 35

if be_verbose: print('# All done')